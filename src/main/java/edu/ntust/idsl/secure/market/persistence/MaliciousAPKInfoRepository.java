package edu.ntust.idsl.secure.market.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.ntust.idsl.secure.market.domain.MaliciousAPKInfo;

/**
 * 
 * @author Carl Lu
 *
 */
@Repository
public interface MaliciousAPKInfoRepository extends JpaRepository<MaliciousAPKInfo, Long> {
	
	List<MaliciousAPKInfo> findByAppName(String appName);
	
	MaliciousAPKInfo findByAppNameAndVersionCode(String appName, String versionCode);
	
	MaliciousAPKInfo findByApkNameAndVersionCodeAndApkFingerprint(String apkName, String versionCode, String apkFingerprint);
	
}
