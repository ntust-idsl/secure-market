package edu.ntust.idsl.secure.market.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.ntust.idsl.secure.market.util.constant.TemplateResource;
import edu.ntust.idsl.secure.market.util.constant.URLResource;

/**
 * 
 * @author Carl Lu
 *
 */
@Controller
@RequestMapping(value = "${url.mapping.root}")
public class EntryController {
	
	@Autowired
	private URLResource url;
	
	@Autowired
	private TemplateResource template;
	
	@RequestMapping(value = "/")
	public String webRoot() {
		return "forward:" + url.getUrlForHome();
	}
	
	@RequestMapping(value = "${url.mapping.home}")
	public String homePage() {
		return template.getWebPageForHome();
	}

}
