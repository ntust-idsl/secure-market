package edu.ntust.idsl.secure.market.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.ntust.idsl.secure.market.util.constant.WebConstant;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;

/**
 * 
 * @author Carl Lu
 *
 */
@Component
public class ProcessBuilderGenerator {
	
	@Autowired
	private WebConstant webConstant;
	
	private String apkName;
	
	private String digitalSignature;

	public ProcessBuilder generateProcessBuilderWithScenario(int scenario) {
		ProcessBuilder processBuilder = new ProcessBuilder();
		processBuilder.command(generateCommandListWithSpecifiedScenario(scenario));
		return processBuilder;
	}

	private List<String> generateCommandListWithSpecifiedScenario(int scenario) {
		List<String> command = null;
		switch (scenario) {
		case ResourceConstant.OBTAIN_APK_MANIFEST_ANALYZER_PROCESS:
			command = obtainCommandListForDecompileAPK();
			break;
		case ResourceConstant.OBTAIN_EXTRACT_DIGITAL_SIGNATURE_PROCESS:
			command = obtainCommandListForExtractDigitalSignature();
			break;
		default:
			break;
		}
		return command;
	}

	private List<String> obtainCommandListForDecompileAPK() {
		List<String> command = new ArrayList<String>();
		command.add(webConstant.getPathToAPKTool() + webConstant.getApkToolCommand());
		command.add(webConstant.getApkToolOptionForDecompile());
		command.add(webConstant.getApkToolOptionForOverwrite());
		command.add(webConstant.getApkToolOptionForMatchOriginal());
		command.add(webConstant.getPathToUploaded() + apkName);
		command.add(webConstant.getApkToolOptionForOutput());
		command.add(webConstant.getPathToDecompiled() + apkName);
		return command;
	}

	private List<String> obtainCommandListForExtractDigitalSignature() {
		List<String> command = new ArrayList<String>();
		command.add(webConstant.getOpensslCommand());
		command.add(webConstant.getOpensslOptionForCryptoStnd());
		command.add(webConstant.getOpensslOptionForInput());
		command.add(digitalSignature);
		command.add(webConstant.getOpensslOptionForPrintCerts());
		command.add(webConstant.getOpensslOptionForInform());
		command.add(webConstant.getOpensslOptionForInformFormat());
		command.add(webConstant.getOpensslOptionForOutput());
		command.add(apkName.concat(ResourceConstant.SUFFIX_FOR_CERTIFICATE));
		return command;
	}

	public void setApkName(String apkName) {
		this.apkName = apkName;
	}

	public void setDigitalSignature(String digitalSignature) {
		this.digitalSignature = digitalSignature;
	}
	
}
