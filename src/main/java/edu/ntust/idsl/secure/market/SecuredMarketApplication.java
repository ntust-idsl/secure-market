package edu.ntust.idsl.secure.market;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 
 * @author Carl Lu
 *
 */
@SpringBootApplication
@EnableScheduling
@PropertySource(value = { "classpath:/config/request-mapping.properties",
						  "classpath:/config/template-mapping.properties",
						  "classpath:/config/command.properties",
						  "classpath:/config/path.properties"})
public class SecuredMarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecuredMarketApplication.class, args);
	}

}
