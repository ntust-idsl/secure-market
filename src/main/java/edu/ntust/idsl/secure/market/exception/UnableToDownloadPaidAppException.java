package edu.ntust.idsl.secure.market.exception;

/**
 * 
 * @author Carl Lu
 *
 */
public class UnableToDownloadPaidAppException extends RuntimeException {

	private static final long serialVersionUID = -7532933442561226490L;

	public UnableToDownloadPaidAppException(String msg) {
		super(msg);
	}
	
	public UnableToDownloadPaidAppException(String msg, Throwable throwable) {
		super(msg, throwable);
	}
	
}
