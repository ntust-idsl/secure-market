package edu.ntust.idsl.secure.market.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.ntust.idsl.secure.market.domain.APKInfo;
import edu.ntust.idsl.secure.market.pagination.PageWrapper;
import edu.ntust.idsl.secure.market.service.APKInfoService;
import edu.ntust.idsl.secure.market.util.constant.TemplateResource;
import edu.ntust.idsl.secure.market.util.constant.URLResource;

/**
 * 
 * @author Carl Lu
 *
 */
@Controller
@RequestMapping(value = "${url.mapping.root}" + "${url.mapping.market}")
public class MarketController {
	
	@Autowired
	private URLResource url;

	@Autowired
	private TemplateResource template;
	
	@Autowired
	private APKInfoService apkInfoService;
	
	@RequestMapping(value = "${url.mapping.market.list}", method = RequestMethod.GET)
	public String listAllApps(Model model, Pageable pageable) {
		PageWrapper<APKInfo> pagedAppList = new PageWrapper<APKInfo>(apkInfoService.listAllApps(pageable), url.urlForListApps);
		model.addAttribute("page", pagedAppList);
		model.addAttribute("apps", pagedAppList.getContent());
		return template.getWebPageForListApps();
	}

}
