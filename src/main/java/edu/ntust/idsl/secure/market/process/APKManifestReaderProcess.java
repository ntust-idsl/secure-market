package edu.ntust.idsl.secure.market.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ntust.idsl.secure.market.util.ProcessBuilderGenerator;
import edu.ntust.idsl.secure.market.util.constant.WebConstant;
import edu.ntust.idsl.secure.market.util.constant.ResourceConstant;
import edu.ntust.idsl.secure.market.util.xml.AndroidManifestReader;

/**
 * 
 * @author Carl Lu
 *
 */
public class APKManifestReaderProcess {
	
	private static final Logger logger = LoggerFactory.getLogger(APKManifestReaderProcess.class);
	
	private WebConstant webConstant;
	
	private AndroidManifestReader androidManifestReader;
	
	private ProcessBuilderGenerator processBuilderGenerator;
	
	private String versionCode;
	
	public APKManifestReaderProcess(WebConstant appConstant, ProcessBuilderGenerator processBuilderGenerator) {
		this.webConstant = appConstant;
		this.processBuilderGenerator = processBuilderGenerator;
	}
	
	public void decompileAPK(String apkName) throws Exception {
		try {
			boolean disassemblingCalssFailed = false;
			processBuilderGenerator.setApkName(apkName);
			ProcessBuilder processBuilderForDecompileAPK = processBuilderGenerator.generateProcessBuilderWithScenario(ResourceConstant.OBTAIN_APK_MANIFEST_ANALYZER_PROCESS);
			Process processForDecompileAPK = processBuilderForDecompileAPK.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(processForDecompileAPK.getInputStream()));
			BufferedReader errorReader = new BufferedReader(new InputStreamReader(processForDecompileAPK.getErrorStream()));
			String decompileAPKContent;
			while ((decompileAPKContent = errorReader.readLine()) != null) {
				if(decompileAPKContent.contains(ResourceConstant.ERROR_MSG_FOR_DISASSEMBLING_CLASS)) {
					disassemblingCalssFailed = true;
				}
			}
			if(disassemblingCalssFailed) {
				logger.error("Error occurred when disassembling class.");
			}
			while ((decompileAPKContent = reader.readLine()) != null) {
				logger.debug(decompileAPKContent);
			}
		} catch (Exception e) {
			logger.error("Something wrong when decompiling {}", apkName);
			throw e;
		}
	}
	
	public void extractVersionCode(String apkName) throws IOException {
		androidManifestReader = new AndroidManifestReader();
		androidManifestReader.loadManifest(webConstant.getPathToDecompiled() + apkName + File.separator + ResourceConstant.ANDROID_MANIFEST_XML);
		versionCode = androidManifestReader.extractVersionCode();
		androidManifestReader.releaseXMLResource();
	}

	public String getVersionCode() {
		return versionCode;
	}

}
