package edu.ntust.idsl.secure.market.util.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Carl Lu
 *
 */
@Component
public class URLResource {

	@Value("${url.mapping.root}")
	public String urlForWebRoot;
	
	@Value("${url.home}")
	public String urlForHome;
	
	@Value("${url.market.list}")
	public String urlForListApps;

	public String getUrlForWebRoot() {
		return urlForWebRoot;
	}

	public String getUrlForHome() {
		return urlForHome;
	}

	public String getUrlForListApps() {
		return urlForListApps;
	}
	
}
