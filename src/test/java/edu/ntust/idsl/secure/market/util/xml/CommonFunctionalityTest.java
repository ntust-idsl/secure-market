package edu.ntust.idsl.secure.market.util.xml;

/**
 * 
 * @author Carl Lu
 *
 */
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.junit.Ignore;
import org.junit.Test;

import edu.ntust.idsl.secure.market.util.FileExplorer;

public class CommonFunctionalityTest {

	@Test
	@Ignore
	public void testForDeleteDir() {
		String dir = "C:\\Secured_Market\\apps\\decompiled\\com.evernote.apk";
		try {
			FileExplorer.deleteFolder(dir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertFalse(new File(dir).exists());
	}
	
	@Test
	@Ignore
	public void testForExamingRsaFile() {
		String dir = "C:\\Secured_Market\\apps\\extract\\META-INF";
		try {
			int size = FileExplorer.searchFileWithSpecificSuffixInDir(dir, ".RSA").size();
			assertEquals(1, size);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@Ignore
	public void testForAppendToFile() {
		Path rn_demo = Paths.get("/Users/Carl/work/Project/Secured_Market", "failed.txt");
        String string = "\nString: from java2s.com";

        //using NIO.2 unbuffered stream
        byte data[] = string.getBytes();
        try (OutputStream outputStream = Files.newOutputStream(rn_demo, 
        		StandardOpenOption.CREATE, 
        		StandardOpenOption.APPEND)) {
            outputStream.write(data);
        } catch (IOException e) {
            System.err.println(e);
        }
	}

}
