package edu.ntust.idsl.secure.market.util;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.ntust.idsl.secure.market.domain.APKInfo;
import edu.ntust.idsl.secure.market.domain.AppComparedResult;
import edu.ntust.idsl.secure.market.domain.enums.MismatchedResource;

/**
 * 
 * @author Carl Lu
 *
 */
public class AppIntegrityVerifierTest {
	
	private static APKInfo verified;
	private static APKInfo unverified;
	private static APKInfo unverified2;
	
	@BeforeClass
	public static void testInit() {
		verified = new APKInfo();
		unverified = new APKInfo();
		unverified2 = new APKInfo();
		
		verified.setId(1l);
		verified.setAppName("怪物彈珠");
		verified.setApkName("jp.co.mixi.monsterstrikeTW.apk");
		verified.setVersionCode("22");
		verified.setMnftEntrySize(1024);
		verified.setAppCertificate("uaNUU/VQFjzABO2ZvvqP7uRJTVw=");
		verified.setAndroidMnftXml("CZmWCgorNs5Xk7wLNTNdYwIiHNE=");
		verified.setResourcesArsc("Wsg1OtrJTVay4H/8WHXxpPdYWbY=");
		verified.setClassesDex("gUpG2glvk2ZhffqHSSQzLUgL0W8=");
		verified.setApkFingerprint("755c9b4319531fc4d82442a9c6726feeb57ac901");
		verified.setUploadTime(LocalDateTime.now());
		
		unverified.setId(2l);
		unverified.setAppName("怪物彈珠");
		unverified.setApkName("jp.co.mixi.monsterstrikeTW.apk");
		unverified.setVersionCode("22");
		unverified.setMnftEntrySize(1024);
		unverified.setAppCertificate("uaNUU/VQFjzABO2ZvvqP7uRJTVw=");
		unverified.setAndroidMnftXml("CZmWCgorNs5Xk7wLNTNdYwIiHNE=");
		unverified.setResourcesArsc("Wsg1OtrJTVay4H/8WHXxpPdYWbY=");
		unverified.setClassesDex("gUpG2glvk2ZhffqHSSQzLUgL0W8=");
		unverified.setApkFingerprint("755c9b4319531fc4d82442a9c6726feeb57ac901");
		unverified.setUploadTime(LocalDateTime.now());
		
		unverified2.setId(3l);
		unverified2.setAppName("怪物彈珠");
		unverified2.setApkName("jp.co.mixi.monsterstrikeTW.apk");
		unverified2.setVersionCode("22");
		unverified2.setMnftEntrySize(1024);
		unverified2.setAppCertificate("uaNUU/VQFjzABO2ZvvqP7uRJTVw=");
		unverified2.setAndroidMnftXml("CZmWCgorNs5Xk7wLNTNdYwIiHNE=");
		unverified2.setResourcesArsc("Wsg1OtrJTVay4H/8WHXxpPdYWbY3");
		unverified2.setClassesDex("gUpG2glvk2ZhffqHSSQzLUgL0W81");
		unverified2.setApkFingerprint("755c9b4319531fc4d82442a9c6726feeb57ac902");
		unverified2.setUploadTime(LocalDateTime.now());
	}

	@Test
	public void testForCorrect() {
		AppComparedResult result = compare(verified, unverified);
		assertTrue(result.isPassed());
		assertEquals(0, result.getMismatchedResources().size());
	}
	
	@Test
	public void testForFailed() {
		AppComparedResult result = compare(verified, unverified2);
		assertFalse(result.isPassed());
		assertEquals(3, result.getMismatchedResources().size());
	}
	
	private AppComparedResult compare(APKInfo verified, APKInfo unverified) {
		AppComparedResult result = new AppComparedResult();
		List<MismatchedResource> mismatchResources = new ArrayList<MismatchedResource>();
		if(!unverified.getAppCertificate().equals(verified.getAppCertificate())) {
			mismatchResources.add(MismatchedResource.CERTIFICATE);
		}
		if(!unverified.getVersionCode().equals(verified.getVersionCode())) {
			mismatchResources.add(MismatchedResource.VERSIONCODE);
		}
		if(!String.valueOf(unverified.getMnftEntrySize()).equals(String.valueOf(verified.getMnftEntrySize()))) {
			mismatchResources.add(MismatchedResource.MNFT_ENTRY_SIZE);
		}
		if(!unverified.getAndroidMnftXml().equals(verified.getAndroidMnftXml())) {
			mismatchResources.add(MismatchedResource.ANDROID_MNFT_XML);
		}
		if(!unverified.getClassesDex().equals(verified.getClassesDex())) {
			mismatchResources.add(MismatchedResource.CLASSES_DEX);
		}
		if(!unverified.getResourcesArsc().equals(verified.getResourcesArsc())) {
			mismatchResources.add(MismatchedResource.RESOURCES_ARSC);
		}
		if(!unverified.getApkFingerprint().equals(verified.getApkFingerprint())) {
			mismatchResources.add(MismatchedResource.LIGHTWEIGHT_APK_FINGER_PRINT);
		}
		result.setMismatchedResources(mismatchResources);
		if(mismatchResources.size() > 0) {
			result.setPassed(false);
		} else {
			result.setPassed(true);
		}
		return result;
	}

}
