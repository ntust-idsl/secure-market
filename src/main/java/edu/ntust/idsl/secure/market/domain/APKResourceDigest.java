package edu.ntust.idsl.secure.market.domain;

import java.io.Serializable;

/**
 * 
 * @author Ssu-Wei Tang, Carl Lu
 *
 */
public class APKResourceDigest implements Serializable {

	private static final long serialVersionUID = -6831758094064913347L;
	
	private String sha1Base64;
	private String sha1Hex;
	private String fileName;
	
	public String getSha1Base64() {
		return sha1Base64;
	}
	public void setSha1Base64(String sha1Base64) {
		this.sha1Base64 = sha1Base64;
	}
	
	public String getSha1Hex() {
		return sha1Hex;
	}
	public void setSha1Hex(String sha1Hex) {
		this.sha1Hex = sha1Hex;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + ((sha1Base64 == null) ? 0 : sha1Base64.hashCode());
		result = prime * result + ((sha1Hex == null) ? 0 : sha1Hex.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		APKResourceDigest other = (APKResourceDigest) obj;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		if (sha1Base64 == null) {
			if (other.sha1Base64 != null)
				return false;
		} else if (!sha1Base64.equals(other.sha1Base64))
			return false;
		if (sha1Hex == null) {
			if (other.sha1Hex != null)
				return false;
		} else if (!sha1Hex.equals(other.sha1Hex))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "APKFingerprint [sha1Base64=" + sha1Base64 + ", sha1Hex="
				+ sha1Hex + ", fileName=" + fileName + "]";
	}

}
