package edu.ntust.idsl.secure.market.domain;

import java.io.Serializable;

/**
 * 
 * @author Ssu-Wei Tang, Carl Lu
 *
 */
public class APKFingerprint implements Serializable {

	private static final long serialVersionUID = -6831758094064913347L;
	
	private Long id;
	private String fingerprintForDigitalSignature;
	private String fingerprintForResXml;
	private String fingerprintForResImg;
	private String fingerprintForClassesDex;
	private String fingerprintForAndroidManifestXml;
	private String fingerprintForResourcesArsc;
	private String fingerprintForRes;
	private String fingerprintForEntireAPKFile;
	private String uploadFileInfo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getFingerprintForDigitalSignature() {
		return fingerprintForDigitalSignature;
	}
	public void setFingerprintForDigitalSignature(
			String fingerprintForDigitalSignature) {
		this.fingerprintForDigitalSignature = fingerprintForDigitalSignature;
	}
	
	public String getFingerprintForResXml() {
		return fingerprintForResXml;
	}
	public void setFingerprintForResXml(String fingerprintForResXml) {
		this.fingerprintForResXml = fingerprintForResXml;
	}
	
	public String getFingerprintForResImg() {
		return fingerprintForResImg;
	}
	public void setFingerprintForResImg(String fingerprintForResImg) {
		this.fingerprintForResImg = fingerprintForResImg;
	}
	
	public String getFingerprintForClassesDex() {
		return fingerprintForClassesDex;
	}
	public void setFingerprintForClassesDex(String fingerprintForClassesDex) {
		this.fingerprintForClassesDex = fingerprintForClassesDex;
	}
	
	public String getFingerprintForAndroidManifestXml() {
		return fingerprintForAndroidManifestXml;
	}
	public void setFingerprintForAndroidManifestXml(
			String fingerprintForAndroidManifestXml) {
		this.fingerprintForAndroidManifestXml = fingerprintForAndroidManifestXml;
	}
	
	public String getFingerprintForResourcesArsc() {
		return fingerprintForResourcesArsc;
	}
	public void setFingerprintForResourcesArsc(String fingerprintForResourcesArsc) {
		this.fingerprintForResourcesArsc = fingerprintForResourcesArsc;
	}
	
	public String getFingerprintForRes() {
		return fingerprintForRes;
	}
	public void setFingerprintForRes(String fingerprintForRes) {
		this.fingerprintForRes = fingerprintForRes;
	}
	
	public String getFingerprintForEntireAPKFile() {
		return fingerprintForEntireAPKFile;
	}
	public void setFingerprintForEntireAPKFile(String fingerprintForEntireAPKFile) {
		this.fingerprintForEntireAPKFile = fingerprintForEntireAPKFile;
	}
	
	public String getUploadFileInfo() {
		return uploadFileInfo;
	}
	public void setUploadFileInfo(String uploadFileInfo) {
		this.uploadFileInfo = uploadFileInfo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fingerprintForAndroidManifestXml == null) ? 0 : fingerprintForAndroidManifestXml.hashCode());
		result = prime * result + ((fingerprintForClassesDex == null) ? 0 : fingerprintForClassesDex.hashCode());
		result = prime * result + ((fingerprintForDigitalSignature == null) ? 0 : fingerprintForDigitalSignature.hashCode());
		result = prime * result + ((fingerprintForEntireAPKFile == null) ? 0 : fingerprintForEntireAPKFile.hashCode());
		result = prime * result + ((fingerprintForRes == null) ? 0 : fingerprintForRes .hashCode());
		result = prime * result + ((fingerprintForResImg == null) ? 0 : fingerprintForResImg.hashCode());
		result = prime * result + ((fingerprintForResXml == null) ? 0 : fingerprintForResXml.hashCode());
		result = prime * result + ((fingerprintForResourcesArsc == null) ? 0 : fingerprintForResourcesArsc.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((uploadFileInfo == null) ? 0 : uploadFileInfo.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		APKFingerprint other = (APKFingerprint) obj;
		if (fingerprintForAndroidManifestXml == null) {
			if (other.fingerprintForAndroidManifestXml != null)
				return false;
		} else if (!fingerprintForAndroidManifestXml.equals(other.fingerprintForAndroidManifestXml))
			return false;
		if (fingerprintForClassesDex == null) {
			if (other.fingerprintForClassesDex != null)
				return false;
		} else if (!fingerprintForClassesDex.equals(other.fingerprintForClassesDex))
			return false;
		if (fingerprintForDigitalSignature == null) {
			if (other.fingerprintForDigitalSignature != null)
				return false;
		} else if (!fingerprintForDigitalSignature.equals(other.fingerprintForDigitalSignature))
			return false;
		if (fingerprintForEntireAPKFile == null) {
			if (other.fingerprintForEntireAPKFile != null)
				return false;
		} else if (!fingerprintForEntireAPKFile.equals(other.fingerprintForEntireAPKFile))
			return false;
		if (fingerprintForRes == null) {
			if (other.fingerprintForRes != null)
				return false;
		} else if (!fingerprintForRes.equals(other.fingerprintForRes))
			return false;
		if (fingerprintForResImg == null) {
			if (other.fingerprintForResImg != null)
				return false;
		} else if (!fingerprintForResImg.equals(other.fingerprintForResImg))
			return false;
		if (fingerprintForResXml == null) {
			if (other.fingerprintForResXml != null)
				return false;
		} else if (!fingerprintForResXml.equals(other.fingerprintForResXml))
			return false;
		if (fingerprintForResourcesArsc == null) {
			if (other.fingerprintForResourcesArsc != null)
				return false;
		} else if (!fingerprintForResourcesArsc.equals(other.fingerprintForResourcesArsc))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (uploadFileInfo == null) {
			if (other.uploadFileInfo != null)
				return false;
		} else if (!uploadFileInfo.equals(other.uploadFileInfo))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "APKFingerprint [id=" + id + ", fingerprintForDigitalSignature="
				+ fingerprintForDigitalSignature + ", fingerprintForResXml="
				+ fingerprintForResXml + ", fingerprintForResImg="
				+ fingerprintForResImg + ", fingerprintForClassesDex="
				+ fingerprintForClassesDex
				+ ", fingerprintForAndroidManifestXml="
				+ fingerprintForAndroidManifestXml
				+ ", fingerprintForResourcesArsc="
				+ fingerprintForResourcesArsc + ", fingerprintForRes="
				+ fingerprintForRes + ", fingerprintForEntireAPKFile="
				+ fingerprintForEntireAPKFile + ", uploadFileInfo="
				+ uploadFileInfo + "]";
	}

}
